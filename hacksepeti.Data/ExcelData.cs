﻿using hacksepeti.Models;
using System.Collections.Generic;

namespace hacksepeti.Data
{
    public class ExcelData
    {
        //provider problemi yaşanabileceği için datayı statik tanımladık.

        public static List<Branch> Branches
        {
            get
            {
                return new List<Branch>
                {
                    new Branch { Name = "Kırmızı", ColorCode="red", Point = new Point{ Latitude=41.049792, Longitude=29.003031 }, Quota = new BranchQuota { Max = 30, Min = 20 } },
                    new Branch { Name = "Yeşil", ColorCode="green", Point = new Point{ Latitude=41.069940, Longitude=29.019250 }, Quota = new BranchQuota { Max = 50, Min = 35 } },
                    new Branch { Name = "Mavi", ColorCode="blue", Point = new Point{ Latitude=41.049997, Longitude=29.026108 }, Quota = new BranchQuota { Max = 80, Min = 20 } }
                };
            }
        }

        public static List<Order> Orders
        {
            get
            {
                return new List<Order>
                {
                    new Order { Id = 100, Point = new Point{ Latitude=41.078260000000, Longitude=29.015770000000 }},
                    new Order { Id = 101, Point = new Point{ Latitude=41.060780000000, Longitude=29.010830000000 }},
                    new Order { Id = 102, Point = new Point{ Latitude=41.064160000000, Longitude=29.009970000000 }},
                    new Order { Id = 103, Point = new Point{ Latitude=41.085290000000, Longitude=29.019790000000 }},
                    new Order { Id = 104, Point = new Point{ Latitude=41.041840000000, Longitude=29.002990000000 }},
                    new Order { Id = 105, Point = new Point{ Latitude=41.085500000000, Longitude=29.008710000000 }},
                    new Order { Id = 106, Point = new Point{ Latitude=41.085040000000, Longitude=29.017570000000 }},
                    new Order { Id = 107, Point = new Point{ Latitude=41.052210000000, Longitude=29.033230000000 }},
                    new Order { Id = 108, Point = new Point{ Latitude=41.081170000000, Longitude=29.039800000000 }},
                    new Order { Id = 109, Point = new Point{ Latitude=41.085960000000, Longitude=29.033490000000 }},
                    new Order { Id = 110, Point = new Point{ Latitude=41.060720000000, Longitude=29.011340000000 }},
                    new Order { Id = 111, Point = new Point{ Latitude=41.070020000000, Longitude=29.017650000000 }},
                    new Order { Id = 112, Point = new Point{ Latitude=41.080520000000, Longitude=29.012240000000 }},
                    new Order { Id = 113, Point = new Point{ Latitude=41.046150000000, Longitude=29.012040000000 }},
                    new Order { Id = 114, Point = new Point{ Latitude=41.051280000000, Longitude=29.003100000000 }},
                    new Order { Id = 115, Point = new Point{ Latitude=41.054340000000, Longitude=29.031720000000 }},
                    new Order { Id = 116, Point = new Point{ Latitude=41.078260000000, Longitude=29.015770000000 }},
                    new Order { Id = 117, Point = new Point{ Latitude=41.061770000000, Longitude=29.037880000000 }},
                    new Order { Id = 118, Point = new Point{ Latitude=41.061480000000, Longitude=29.004810000000 }},
                    new Order { Id = 119, Point = new Point{ Latitude=41.046830000000, Longitude=29.003130000000 }},
                    new Order { Id = 120, Point = new Point{ Latitude=41.042900000000, Longitude=29.000040000000 }},
                    new Order { Id = 121, Point = new Point{ Latitude=41.045500000000, Longitude=28.999780000000 }},
                    new Order { Id = 122, Point = new Point{ Latitude=41.051280000000, Longitude=29.003100000000 }},
                    new Order { Id = 123, Point = new Point{ Latitude=41.045500000000, Longitude=28.999780000000 }},
                    new Order { Id = 124, Point = new Point{ Latitude=41.059380000000, Longitude=29.000360000000 }},
                    new Order { Id = 125, Point = new Point{ Latitude=41.077080000000, Longitude=29.031190000000 }},
                    new Order { Id = 126, Point = new Point{ Latitude=41.047130000000, Longitude=29.009110000000 }},
                    new Order { Id = 127, Point = new Point{ Latitude=41.079980000000, Longitude=29.021330000000 }},
                    new Order { Id = 128, Point = new Point{ Latitude=41.064160000000, Longitude=29.009970000000 }},
                    new Order { Id = 129, Point = new Point{ Latitude=41.070920000000, Longitude=29.042000000000 }},
                    new Order { Id = 130, Point = new Point{ Latitude=41.083220000000, Longitude=29.013630000000 }},
                    new Order { Id = 131, Point = new Point{ Latitude=41.066040000000, Longitude=29.007370000000 }},
                    new Order { Id = 132, Point = new Point{ Latitude=41.080850000000, Longitude=29.010360000000 }},
                    new Order { Id = 133, Point = new Point{ Latitude=41.043780000000, Longitude=29.009960000000 }},
                    new Order { Id = 134, Point = new Point{ Latitude=41.064160000000, Longitude=29.009970000000 }},
                    new Order { Id = 135, Point = new Point{ Latitude=41.066000000000, Longitude=29.040690000000 }},
                    new Order { Id = 136, Point = new Point{ Latitude=41.086880000000, Longitude=29.017910000000 }},
                    new Order { Id = 137, Point = new Point{ Latitude=41.051750000000, Longitude=28.990540000000 }},
                    new Order { Id = 138, Point = new Point{ Latitude=41.059280000000, Longitude=29.012050000000 }},
                    new Order { Id = 139, Point = new Point{ Latitude=41.079220000000, Longitude=29.010440000000 }},
                    new Order { Id = 140, Point = new Point{ Latitude=41.054610000000, Longitude=29.027400000000 }},
                    new Order { Id = 141, Point = new Point{ Latitude=41.063070000000, Longitude=29.010960000000 }},
                    new Order { Id = 142, Point = new Point{ Latitude=41.075950000000, Longitude=29.015770000000 }},
                    new Order { Id = 143, Point = new Point{ Latitude=41.043780000000, Longitude=29.009960000000 }},
                    new Order { Id = 144, Point = new Point{ Latitude=41.067660000000, Longitude=29.043050000000 }},
                    new Order { Id = 145, Point = new Point{ Latitude=41.056190000000, Longitude=28.999960000000 }},
                    new Order { Id = 146, Point = new Point{ Latitude=41.079190000000, Longitude=29.045160000000 }},
                    new Order { Id = 147, Point = new Point{ Latitude=41.077080000000, Longitude=29.031190000000 }},
                    new Order { Id = 148, Point = new Point{ Latitude=41.057970000000, Longitude=29.016160000000 }},
                    new Order { Id = 149, Point = new Point{ Latitude=41.092640000000, Longitude=29.018040000000 }},
                    new Order { Id = 150, Point = new Point{ Latitude=41.060780000000, Longitude=29.010830000000 }},
                    new Order { Id = 151, Point = new Point{ Latitude=41.077360000000, Longitude=29.030970000000 }},
                    new Order { Id = 152, Point = new Point{ Latitude=41.048580000000, Longitude=29.021250000000 }},
                    new Order { Id = 153, Point = new Point{ Latitude=41.053072478048, Longitude=28.987209066283 }},
                    new Order { Id = 154, Point = new Point{ Latitude=41.083100000000, Longitude=29.015150000000 }},
                    new Order { Id = 155, Point = new Point{ Latitude=41.060780000000, Longitude=29.010830000000 }},
                    new Order { Id = 156, Point = new Point{ Latitude=41.052430000000, Longitude=28.990450000000 }},
                    new Order { Id = 157, Point = new Point{ Latitude=41.063710000000, Longitude=29.010420000000 }},
                    new Order { Id = 158, Point = new Point{ Latitude=41.083290000000, Longitude=29.028520000000 }},
                    new Order { Id = 159, Point = new Point{ Latitude=41.053670000000, Longitude=28.989390000000 }},
                    new Order { Id = 160, Point = new Point{ Latitude=41.076840000000, Longitude=29.015430000000 }},
                    new Order { Id = 161, Point = new Point{ Latitude=41.056950000000, Longitude=29.012310000000 }},
                    new Order { Id = 162, Point = new Point{ Latitude=41.078880000000, Longitude=29.030180000000 }},
                    new Order { Id = 163, Point = new Point{ Latitude=41.067730000000, Longitude=29.020290000000 }},
                    new Order { Id = 164, Point = new Point{ Latitude=41.063070000000, Longitude=29.010960000000 }},
                    new Order { Id = 165, Point = new Point{ Latitude=41.063070000000, Longitude=29.010960000000 }},
                    new Order { Id = 166, Point = new Point{ Latitude=41.045950000000, Longitude=28.999860000000 }},
                    new Order { Id = 167, Point = new Point{ Latitude=41.076840000000, Longitude=29.015430000000 }},
                    new Order { Id = 168, Point = new Point{ Latitude=41.061600000000, Longitude=29.000460000000 }},
                    new Order { Id = 169, Point = new Point{ Latitude=41.064380000000, Longitude=29.001360000000 }},
                    new Order { Id = 170, Point = new Point{ Latitude=41.056890000000, Longitude=28.987130000000 }},
                    new Order { Id = 171, Point = new Point{ Latitude=41.074350000000, Longitude=29.039250000000 }},
                    new Order { Id = 172, Point = new Point{ Latitude=41.051820000000, Longitude=29.005970000000 }},
                    new Order { Id = 173, Point = new Point{ Latitude=41.058420000000, Longitude=29.007200000000 }},
                    new Order { Id = 174, Point = new Point{ Latitude=41.052080000000, Longitude=29.007210000000 }},
                    new Order { Id = 175, Point = new Point{ Latitude=41.078910000000, Longitude=29.021220000000 }},
                    new Order { Id = 176, Point = new Point{ Latitude=41.057180000000, Longitude=29.030470000000 }},
                    new Order { Id = 177, Point = new Point{ Latitude=41.066040000000, Longitude=29.007370000000 }},
                    new Order { Id = 178, Point = new Point{ Latitude=41.087650000000, Longitude=29.007350000000 }},
                    new Order { Id = 179, Point = new Point{ Latitude=41.083680000000, Longitude=29.014990000000 }},
                    new Order { Id = 180, Point = new Point{ Latitude=41.089100000000, Longitude=29.035370000000 }},
                    new Order { Id = 181, Point = new Point{ Latitude=41.084760000000, Longitude=29.013510000000 }},
                    new Order { Id = 182, Point = new Point{ Latitude=41.041840000000, Longitude=29.002990000000 }},
                    new Order { Id = 183, Point = new Point{ Latitude=41.061950000000, Longitude=29.011850000000 }},
                    new Order { Id = 184, Point = new Point{ Latitude=41.053340000000, Longitude=29.003820000000 }},
                    new Order { Id = 185, Point = new Point{ Latitude=41.074380000000, Longitude=29.018530000000 }},
                    new Order { Id = 186, Point = new Point{ Latitude=41.080440000000, Longitude=29.015540000000 }},
                    new Order { Id = 187, Point = new Point{ Latitude=41.061130000000, Longitude=28.998080000000 }},
                    new Order { Id = 188, Point = new Point{ Latitude=41.060780000000, Longitude=29.010830000000 }},
                    new Order { Id = 189, Point = new Point{ Latitude=41.075650000000, Longitude=29.020470000000 }},
                    new Order { Id = 190, Point = new Point{ Latitude=41.076840000000, Longitude=29.015430000000 }},
                    new Order { Id = 191, Point = new Point{ Latitude=41.048590000000, Longitude=29.021570000000 }},
                    new Order { Id = 192, Point = new Point{ Latitude=41.066040000000, Longitude=29.007370000000 }},
                    new Order { Id = 193, Point = new Point{ Latitude=41.078476490079, Longitude=29.009396156746 }},
                    new Order { Id = 194, Point = new Point{ Latitude=41.079370000000, Longitude=29.011590000000 }},
                    new Order { Id = 195, Point = new Point{ Latitude=41.084650000000, Longitude=29.020610000000 }},
                    new Order { Id = 196, Point = new Point{ Latitude=41.066040000000, Longitude=29.007370000000 }},
                    new Order { Id = 197, Point = new Point{ Latitude=41.066440000000, Longitude=29.004270000000 }},
                    new Order { Id = 198, Point = new Point{ Latitude=41.072770000000, Longitude=29.016660000000 }},
                    new Order { Id = 199, Point = new Point{ Latitude=41.049030000000, Longitude=29.023410000000 }}
                };
            }
        }
    }
}
