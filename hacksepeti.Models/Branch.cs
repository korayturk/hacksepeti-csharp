﻿using System.Collections.Generic;

namespace hacksepeti.Models
{
    public class Branch
    {
        public Branch()
        {
            Orders = new List<Order>();
        }

        public string Name { get; set; }
        public string ColorCode { get; set; }
        public Point Point { get; set; }
        public BranchQuota Quota { get; set; }

        public List<Order> Orders { get; set; }
        public List<Point> RoutePath { get; set; }
    }
}
