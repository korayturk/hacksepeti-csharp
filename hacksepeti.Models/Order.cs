﻿using System;
using System.Collections.Generic;
using System.Text;

namespace hacksepeti.Models
{
    public class Order
    {
        public Order()
        {
            SortedBranchListByNearestDistance = new List<Branch>();
        }

        public int Id { get; set; }
        public Point Point { get; set; }

        public List<Branch> SortedBranchListByNearestDistance { get; set; }
               

        public string WriteInfo()
        {
            return $"Id: {this.Id}, Point: {Point.Latitude},{Point.Longitude}";
        }
    }
}
