﻿using GeoCoordinatePortable;
using hacksepeti.Models;
using System.Collections.Generic;
using System.Linq;

namespace hacksepeti.Core
{
    public class Functions
    {
        /// <summary>
        /// iki koordinat arası mesafe
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public static double GetDistance(Point from, Point to)
        {

            var firstCordinate = new GeoCoordinate(from.Latitude, from.Longitude);
            var secondCordinate = new GeoCoordinate(to.Latitude, to.Longitude);

            double distance = firstCordinate.GetDistanceTo(secondCordinate);
            return distance;
        }

        /// <summary>
        /// her sipariş için şube listesi hazırlar. hazırladığı şube listesi mesafeye göre yakından uzağa sıralıdır.
        /// </summary>
        /// <param name="orders"></param>
        /// <param name="branches"></param>
        /// <returns></returns>
        public static List<Order> SortBranchByOrder(List<Order> orders, List<Branch> branches)
        {
            foreach (var item in orders)
            {
                item.SortedBranchListByNearestDistance = SortByNearestDistance(item.Point, branches);
            }
            return orders;
        }

        public static List<Branch> SortByNearestDistance(Point orderPoint, List<Branch> branchList)
        {
            return branchList.OrderBy(x => GetDistance(orderPoint, x.Point)).ToList();
        }

        public static List<Point> GetRoutePath(Point start, List<Point> points)
        {
            Point[] copied = new Point[points.Count];
            points.CopyTo(copied);

            List<Point> newList = copied.ToList();
            List<Point> routePath = new List<Point> { start };

            var newlistCount = newList.Count;
            for (int i = 0; i < newlistCount; i++)
            {
                var lastPoint = routePath.LastOrDefault();
                if (lastPoint != null)
                {
                    var nearlestPoint = newList.OrderBy(x => GetDistance(lastPoint, x)).FirstOrDefault();
                    routePath.Add(nearlestPoint);
                    newList.Remove(nearlestPoint);
                }
            }
            return routePath;
        }

        public static double GetTotalDistance(Point start, List<Point> points)
        {
            //Point[] copied = new Point[points.Count];
            //points.CopyTo(copied);

            //List<Point> newList = copied.ToList();
            //List<Point> routePath = new List<Point> { start };

            //var newlistCount = newList.Count;
            //for (int i = 0; i < newlistCount; i++)
            //{
            //    var lastPoint = routePath.LastOrDefault();
            //    if (lastPoint != null)
            //    {
            //        var nearlestPoint = newList.OrderBy(x => GetDistance(lastPoint, x)).FirstOrDefault();
            //        routePath.Add(nearlestPoint);
            //        newList.Remove(nearlestPoint);
            //    }
            //}
            var routePath = GetRoutePath(start, points);

            double totalDistance = 0;
            for (int i = 0; i < routePath.Count; i++)
            {
                if (i + 1 < routePath.Count)
                    totalDistance += GetDistance(routePath[i], routePath[i + 1]);
            }

            return totalDistance;
        }
    }
}
