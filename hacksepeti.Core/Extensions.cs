﻿using hacksepeti.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace hacksepeti.Core
{
    public static class Extensions
    {
        public static string WriteList(this List<Branch> branches)
        {
            return string.Join("\n", branches.Select(x => $"Name: {x.Name}, Point: {x.Point.Latitude}, {x.Point.Longitude}").ToArray());
        }

        public static string WriteList(this List<Branch> branches, Point orderPoint)
        {
            return string.Join("\n", branches.Select(x => $"Name: {x.Name}, Point: {x.Point.Latitude}, {x.Point.Longitude}, Distance: {Functions.GetDistance(x.Point, orderPoint)}").ToArray());
        }

        public static Branch FindNearestBranch(this Order order, List<Branch> branches, Func<Branch, bool> predicate = null)
        {
            if (predicate != null)
                return branches.Where(predicate).OrderBy(x => Functions.GetDistance(order.Point, x.Point)).FirstOrDefault();
            else
                return branches.OrderBy(x => Functions.GetDistance(order.Point, x.Point)).FirstOrDefault();
        }

        public static Branch WhichBranchAssignment(this Order order, ref List<Branch> branches)
        {
            foreach (var branch in branches)
            {
                if (branch.Orders.Any(x => x.Id == order.Id))
                    return branch;
            }

            throw new Exception("Sipariş hiç bir şubeye atanmamış");
        }
    }
}
