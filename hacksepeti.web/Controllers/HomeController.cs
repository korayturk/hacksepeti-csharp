﻿using hacksepeti.Core;
using hacksepeti.Data;
using hacksepeti.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace hacksepeti.web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Map()
        {
            var branches = ExcelData.Branches;
            var orders = ExcelData.Orders.OrderByDescending(x => Functions.GetDistance(x.Point, x.FindNearestBranch(branches).Point)).ToList();
            var assignmentOrderIds = new List<int>();


            //merkeze en uzak siparişleri, şube kotaları dolmadan önce atamak için, en yakın şubelerine atama işlemi
            for (int i = 0; i < 20; i++)
            {
                foreach (var order in orders.Where(x => !assignmentOrderIds.Contains(x.Id)))
                {
                    var branch = order.FindNearestBranch(branches);
                    if (branch.Quota.Min > branch.Orders.Count)
                    {
                        branch.Orders.Add(order);
                        assignmentOrderIds.Add(order.Id);
                    }
                }
            }


            //minimum kotayı dolduramayan şubeler için
            foreach (var branch in branches.Where(x => x.Orders.Count < x.Quota.Min).OrderBy(x => x.Orders.Count))
            {
                var _orders = orders.Where(x => !assignmentOrderIds.Contains(x.Id)).OrderBy(x => Functions.GetDistance(branch.Point, x.Point)).Take(branch.Quota.Min - branch.Orders.Count).ToList();
                assignmentOrderIds.AddRange(_orders.Select(x => x.Id));
                branch.Orders.AddRange(_orders);
            }

            //minimum kotayı doldurduktan sonra 
            foreach (var branch in branches.Where(x => x.Orders.Count < x.Quota.Max).OrderByDescending(x => x.Orders.Count))
            {
                var _orders = orders.Where(x => !assignmentOrderIds.Contains(x.Id)).OrderBy(x => Functions.GetDistance(branch.Point, x.Point)).Take(branch.Quota.Max - branch.Orders.Count).ToList();
                assignmentOrderIds.AddRange(_orders.Select(x => x.Id));
                branch.Orders.AddRange(_orders);
            }

            //değiş tokuş algoritması
            foreach (var branch in branches)
            {
                var otherBranches = branches.Where(x => x.Name != branch.Name);
                var tempOrders = new List<Order>();
                otherBranches.Select(x => x.Orders).ToList().ForEach(x => tempOrders.AddRange(x));

                foreach (var order in tempOrders)
                {
                    var tempBranch = order.FindNearestBranch(branches);
                    if (tempBranch.Name == branch.Name)
                    {
                        var wichBranch = order.WhichBranchAssignment(ref branches);
                        branch.Orders.Add(order);
                        wichBranch.Orders.Remove(order);


                        var transOrder = branch.Orders.OrderBy(x => Functions.GetDistance(x.Point, wichBranch.Point)).FirstOrDefault();
                        if (transOrder != null)
                        {
                            wichBranch.Orders.Add(transOrder);
                            branch.Orders.Remove(transOrder);
                        }
                        else throw new Exception("algoritman yanlış");
                    }
                }
            }


            //en az yol giden şubeye biraz daha sipariş vereceğiz
            var lazyBranch = branches.OrderBy(x => Functions.GetTotalDistance(x.Point, x.Orders.Select(y => y.Point).ToList())).FirstOrDefault();
            List<Order> otherOrders = new List<Order>();
            branches.Where(x => x.Name != lazyBranch.Name).Select(x => x.Orders).ToList().ForEach(x => otherOrders.AddRange(x));

            foreach (var order in otherOrders)
            {
                var nearestOrder = lazyBranch.Orders.Where(x => Functions.GetDistance(x.Point, order.Point) < 200).ToList();

                if (nearestOrder.Count > 0 && lazyBranch.Orders.Count < lazyBranch.Quota.Max)
                {
                    lazyBranch.Orders.Add(order);

                    var whichBranch = order.WhichBranchAssignment(ref branches);
                    whichBranch.Orders.Remove(order);
                }
            }


            var alertMessage = "";
            //routepathlerin belirlenmesi
            foreach (var branch in branches)
            {
                branch.RoutePath = Functions.GetRoutePath(branch.Point, branch.Orders.Select(x => x.Point).ToList());
                alertMessage += $"{branch.Name} Şube: { (Functions.GetTotalDistance(branch.Point, branch.Orders.Select(x => x.Point).ToList()) / 1000).ToString("0.##")} km yol yaptı.\\n";
            }

           
            ViewBag.alert = alertMessage;

            return View(branches);
        }
    }
}